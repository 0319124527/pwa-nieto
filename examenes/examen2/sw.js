const CACHE_NAME = 'nfl-app-cache-v1';
const IMAGEN = '/examenes/examen2/logo.jpg'; // Ruta de la imagen de reemplazo
const SIMBOLO = '★'; // Símbolo especial a elegir

// Archivos a cachear
const urlsToCache = [
  '/',
  IMAGEN
];

// Instalación del Service Worker y cacheo de archivos
self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => cache.addAll(urlsToCache))
  );
});

// Intercepta las solicitudes y sirve desde el caché si está disponible, de lo contrario, solicita al servidor
self.addEventListener('fetch', event => {
  const request = event.request;

  // Si la solicitud es para una imagen, responde con la imagen de reemplazo de la NFL
  if (request.url.endsWith('.jpg') || request.url.endsWith('.png') || request.url.endsWith('.jpeg')) {
    event.respondWith(
      caches.match(IMAGEN)
        .then(response => {
          if (response) {
            return response;
          }
        })
    );
    return;
  }

  // Si la solicitud es para la lista de pendientes, intercepta y modifica la respuesta
  if (request.url.includes('https://jsonplaceholder.typicode.com/todos')) {
    event.respondWith(
      fetch(request)
        .then(response => {
          // Clonar la respuesta para poder usarla en cache y luego retornarla
          const clonedResponse = response.clone();

          // Interceptar los IDs de los pendientes y agregar el símbolo especial
          if (response.ok) {
            const contentType = response.headers.get('content-type');
            if (contentType && contentType.includes('application/json')) {
              return response.json().then(data => {
                const modifiedData = data.map(todo => {
                  return {
                    ...todo,
                    id: `${todo.id}${SIMBOLO}`
                  };
                });
                return new Response(JSON.stringify(modifiedData), {
                  headers: {'content-type': 'application/json'}
                });
              });
            }
          }

          return response;
        })
        .catch(error => {
          console.error('Error fetching todos:', error);
          throw error;
        })
    );
    return;
  }

  // Continuar con el flujo normal de solicitudes
  event.respondWith(
    caches.match(request)
      .then(response => {
        return response || fetch(request);
      })
  );
});

// Elimina las cachés antiguas cuando se activa el Service Worker
self.addEventListener('activate', event => {
  event.waitUntil(
    caches.keys()
      .then(cacheNames => {
        return Promise.all(
          cacheNames.map(cacheName => {
            if (cacheName !== CACHE_NAME) {
              return caches.delete(cacheName);
            }
          })
        );
      })
  );
});