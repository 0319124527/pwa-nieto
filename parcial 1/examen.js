function init() {
    // Funcion que nos ayuda a cargar el script en el documento HTML
}

async function fetchTodos() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    const todos = await response.json();
    return todos;
}

function displayTodoIDs() {
    fetchTodos()
        .then(todos => {
            const ids = todos.map(todo => todo.id);
            alert('Lista de IDs de todos los pendientes: ' + ids.join(', '));
        });
}

function displayTodoIDTitle() {
    fetchTodos()
        .then(todos => {
            const idPendientes = todos.map(todo => `ID: ${todo.id}, Título: ${todo.title}`);
            alert('Lista de IDs y títulos de todos los pendientes: \n' + idPendientes.join('\n'));
        });
}

function displayTodoUnresolved() {
    fetchTodos()
        .then(todos => {
            const sinResolver = todos.filter(todo => !todo.completed);
            const sinResolverP = sinResolver.map(todo => `ID: ${todo.id}, Título: ${todo.title}`);
            alert('Lista de pendientes sin resolver: \n' + sinResolverP.join('\n'));
        });
}

function displayTodoResolved() {
    fetchTodos()
        .then(todos => {
            const resueltos = todos.filter(todo => todo.completed);
            const resueltosP = resueltos.map(todo => `ID: ${todo.id}, Título: ${todo.title}`);
            alert('Lista de pendientes resueltos: \n' + resueltosP.join('\n'));
        });
}

function displayTodoUserID() {
    fetchTodos()
        .then(todos => {
            const userIdP = todos.map(todo => `ID: ${todo.id}, userID: ${todo.userId}`);
            alert('Lista de IDs y userID de todos los pendientes: \n' + userIdP.join('\n'));
        });
}

function displayTodoUnresolvedUserID() {
    fetchTodos()
        .then(todos => {
            const sinResolver = todos.filter(todo => !todo.completed);
            const sinResolverPU = sinResolver.map(todo => `ID: ${todo.id}, userID: ${todo.userId}`);
            alert('Lista de pendientes sin resolver con userID: \n' + sinResolverPU.join('\n'));
        });
}

function displayTodoResolvedUserID() {
    fetchTodos()
        .then(todos => {
            const resueltos = todos.filter(todo => todo.completed);
            const resueltosPU = resueltos.map(todo => `ID: ${todo.id}, userID: ${todo.userId}`);
            alert('Lista de pendientes resueltos con userID: \n' + resueltosPU.join('\n'));
        });
}

